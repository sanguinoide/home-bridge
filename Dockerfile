FROM node:10-alpine

RUN apk add --update --no-cache make python g++

RUN addgroup -S homebridge && adduser -S homebridge -G homebridge
USER homebridge

RUN mkdir ~/.npm-global && npm config set prefix '~/.npm-global'

ENV PATH=~/.npm-global/bin:$PATH

ENV HOMEBRIDGE_VERSION=0.4.46
RUN npm install -g --unsafe-perm homebridge@${HOMEBRIDGE_VERSION}

ENV CONFIG_UI_VERSION=3.9.7
RUN npm install -g --unsafe-perm homebridge-config-ui-x@${CONFIG_UI_VERSION}

RUN mkdir -p ~/.homebridge
COPY config.json /home/homebridge/.homebridge/

CMD /home/homebridge/.npm-global/bin/homebridge -I

EXPOSE 8080